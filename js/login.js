const host = 'http://10.162.108.177:8080';

const logar = function(){
    let body = {
        login: document.querySelector('input[name="login"]').value,
        senha: document.querySelector('input[name="senha"]').value
    }
    
    post(body);
}

const post = function(body){
    fetch(host + '/login', {
        method: 'POST',
        headers: {
            'Content-type': 'application/json'
        },
        body: JSON.stringify(body)
    }).then(resposta => resposta.json()).then((dados) => {
        window.location = 'index.html';
        localStorage.setItem('token', dados);
    }).catch(() => {
        alert('Erro ao fazer login');
    })
}

document.querySelector('main button').onclick = logar;