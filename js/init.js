$('select').formSelect(); 
$('.modal').modal(); 

const tableBody = document.querySelector('.table-body');

let lista = [
  {
    titulo:'Harry Potter',
    autor:'JK Rowling'
  },
  {
    titulo:'Eragon',
    autor:'Cristopher Paolini'
  },
  {
    titulo:'Harry Potter',
    autor:'JK Rowling'
  },
  {
    titulo:'Eragon',
    autor:'Cristopher Paolini'
  },
  {
    titulo:'Harry Potter',
    autor:'JK Rowling'
  },
  {
    titulo:'Eragon',
    autor:'Cristopher Paolini'
  },
  {
    titulo:'Harry Potter',
    autor:'JK Rowling'
  },
  {
    titulo:'Eragon',
    autor:'Cristopher Paolini'
  },
];

const tratarDados = function (resposta) {
  return resposta.json();
}

const pegarLista = function (dados) {
  console.log(dados);
  tableBody.innerHTML = '';

  for(let item of lista) {
    tableBody.innerHTML += `
    <tr>
      <td>${item.titulo}</td>  
      <td>${item.autor}</td>  
      <td><i class="fas fa-exchange-alt tread"></i></td>  
    </tr>`;
  }

  let treads = document.querySelectorAll('.tread'); 

  for (let tread of treads) {
    tread.onclick = function () { 
      $('#modal1').modal('open');
    }
  }
}

fetch('http://10.162.108.177/livros').then(tratarDados).then(pegarLista);


pegarLista(); 
